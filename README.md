easyvim
=======

This is just my personalized VIM Installer.
I copied the idea from:
- SPF13 (http://vim.spf13.com)
- fisa-vim-config (https://github.com/fisadev/fisa-vim-config)

If there's any license issue, please inform me.

Thanks. (^.^)


INSTALLATION
===========

** I COPIED THE IDEA FROM SPF13 **

curl https://gitlab.com/ssiangge/easyvim/raw/master/bootstrap.sh -L -o - | bash
